import React from "react";
import { Route, Routes } from "react-router-dom";
import AboutUs from "./component/AboutUs";
import { FacebookLoginComponent } from "./component/FacebookLoginComponent";
import HomeComponent from "./component/HomeComponent";
import UsVision from "./component/UsVision";

export const Router = ({data}) => {
  return (
    <Routes>
      <Route path="/" element={<HomeComponent data={data} />} />
      <Route path="/aboutUs" element={<AboutUs />} />
      <Route path="/ourVision" element={<UsVision />} />
      <Route path="/login" element={<FacebookLoginComponent />} />
    </Routes>
  );
};
