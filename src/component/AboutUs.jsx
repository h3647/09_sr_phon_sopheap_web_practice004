import React from 'react'
import { Col, Row, Card } from 'react-bootstrap'
import aboutUs from '../images/aboutUs.jpg'

export default function AboutUs() {
  return (
    <div style={{ marginTop: '50px', textAlign: 'justify' }}>
      <Row>
        <Col>
          <Card>
            <Card.Img variant="top" src={aboutUs} />
          </Card>
        </Col>
        <Col>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, aliquid aut iusto omnis rerum illo a porro inventore fugiat optio totam labore aspernatur laudantium culpa ex obcaecati neque, ullam dicta!
            Minus nemo rem eveniet quo cum. Beatae nam assumenda porro quos doloribus odit veniam earum aut deleniti repudiandae labore cupiditate sunt, minima nisi? Itaque autem quia culpa aspernatur dicta nobis?
            Distinctio ipsam eaque dolore! Itaque deleniti asperiores reprehenderit ipsa necessitatibus, ab eaque excepturi voluptas nam voluptate fuga beatae commodi officia? Autem qui sunt distinctio ab eligendi doloribus voluptatum, amet dicta!
            Distinctio ipsam eaque dolore! Itaque deleniti asperiores reprehenderit ipsa necessitatibus, ab eaque excepturi voluptas nam voluptate fuga beatae commodi officia? Autem qui sunt distinctio ab eligendi doloribus voluptatum, amet dicta!
            Aut consectetur dolor hic voluptate soluta. Exercitationem vitae reiciendis nam, quod saepe eius inventore temporibus praesentium ipsa eum, ex doloremque ducimus deleniti animi debitis illo nihil impedit similique accusamus dicta.</p>
        </Col>
      </Row>
      <Row style={{ marginTop: '40px' }}>
        <Col>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, aliquid aut iusto omnis rerum illo a porro inventore fugiat optio totam labore aspernatur laudantium culpa ex obcaecati neque, ullam dicta!
            Minus nemo rem eveniet quo cum. Beatae nam assumenda porro quos doloribus odit veniam earum aut deleniti repudiandae labore cupiditate sunt, minima nisi? Itaque autem quia culpa aspernatur dicta nobis?
            Distinctio ipsam eaque dolore! Itaque deleniti asperiores reprehenderit ipsa necessitatibus, ab eaque excepturi voluptas nam voluptate fuga beatae commodi officia? Autem qui sunt distinctio ab eligendi doloribus voluptatum, amet dicta!
            Aut consectetur dolor hic voluptate soluta. Exercitationem vitae reiciendis nam, quod saepe eius inventore temporibus praesentium ipsa eum, ex doloremque ducimus deleniti animi debitis illo nihil impedit similique accusamus dicta.
            Aut consectetur dolor hic voluptate soluta. Exercitationem vitae reiciendis nam, quod saepe eius inventore temporibus praesentium ipsa eum, ex doloremque ducimus deleniti animi debitis illo nihil impedit similique accusamus dicta.</p>
        </Col>
        <Col>
          <Card>
            <Card.Img variant="top" src={aboutUs} />
          </Card>
        </Col>
      </Row>
      <Row style={{ marginTop: '40px'}}>
        <Col>
          <Card>
            <Card.Img variant="top" src={aboutUs} />
          </Card>
        </Col>
        <Col>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, aliquid aut iusto omnis rerum illo a porro inventore fugiat optio totam labore aspernatur laudantium culpa ex obcaecati neque, ullam dicta!
            Minus nemo rem eveniet quo cum. Beatae nam assumenda porro quos doloribus odit veniam earum aut deleniti repudiandae labore cupiditate sunt, minima nisi? Itaque autem quia culpa aspernatur dicta nobis?
            Distinctio ipsam eaque dolore! Itaque deleniti asperiores reprehenderit ipsa necessitatibus, ab eaque excepturi voluptas nam voluptate fuga beatae commodi officia? Autem qui sunt distinctio ab eligendi doloribus voluptatum, amet dicta!
            Distinctio ipsam eaque dolore! Itaque deleniti asperiores reprehenderit ipsa necessitatibus, ab eaque excepturi voluptas nam voluptate fuga beatae commodi officia? Autem qui sunt distinctio ab eligendi doloribus voluptatum, amet dicta!
            Aut consectetur dolor hic voluptate soluta. Exercitationem vitae reiciendis nam, quod saepe eius inventore temporibus praesentium ipsa eum, ex doloremque ducimus deleniti animi debitis illo nihil impedit similique accusamus dicta.</p>
        </Col>
      </Row>

    </div>
  )
}
