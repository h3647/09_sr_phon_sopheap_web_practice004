import React, { useEffect, useState } from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { useTranslation } from "react-i18next";
import { NavLink } from "react-router-dom";

export default function NavbarComponent() {
  const { t, i18n } = useTranslation();
  const [lang, setLang] = useState("");
  const handleChangeLang = (langCode) => {
    setLang(langCode);
    // i18n.changeLanguage(langCode)
  };
  useEffect(() => {
    i18n.changeLanguage(lang);
  }, [lang]);

  return (
    <div>
      <Navbar bg="white" variant="light">
        <Container>
          <Navbar.Brand>Practice-4</Navbar.Brand>
          <Nav>
            <Nav.Link as={NavLink} to="/" className="Active">
              {t("home")}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/aboutUs" className="Active">
              {t('about')}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/ourVision" className="Active">
              {t('vision')}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/login" className="Active">
              {/* {t('vision')} */}
              login
            </Nav.Link>
            <NavDropdown title="Language" id="basic-nav-dropdown">
              <NavDropdown.Item onClick={() => handleChangeLang("km")}>
                ភាសាខ្មែរ
              </NavDropdown.Item>
              <NavDropdown.Item onClick={() => handleChangeLang("en")}>
                English
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
}
