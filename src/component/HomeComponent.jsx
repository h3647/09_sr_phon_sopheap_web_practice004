import React from 'react'
import { Button, Card, Row, Col } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'

export default function HomeComponent({ data }) {
  const {t} = useTranslation()
  return (
    <div>
      <Row style={{marginTop: '50px'}}>
        {data.map((item) => {
          return (
            <Col>
              <Card style={{ width: '18rem' }}>
                <Card.Img src={item.thumbnail} />
                <Card.Body>
                  <Card.Title>{item.title}</Card.Title>
                  <Card.Text>
                    {item.description}
                  </Card.Text>
                  <Button variant="success">{t('buy')}</Button>
                </Card.Body>
              </Card>
            </Col>
          )
        })}
      </Row>
    </div>
  )
}
