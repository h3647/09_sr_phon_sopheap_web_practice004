import React from 'react'
import { Row, Col } from 'react-bootstrap'
import logo from '../images/logoKSHRD.png'

export default function FooterComponent() {
  return (
    <div style={{marginTop: '15vw', marginBottom: '50px'}}>
      <Row style={{ textAlign: 'center' }}>
        <Col>
          <img src={logo} style={{ width: '100px', height: '130px' }} />
          <h6><b>រក្សាសិទ្ធគ្រប់យ៉ាងដោយ KSHRD Center ឆ្នាំ 2022</b></h6>
        </Col>
        <Col>
          <h4>Address Contact</h4>
          <span>Address: #12, St 323, Sangkat Boeung Kak II, Khan Toul Kork, Tel: 012 998 919 (Khmer)</span><br></br>
          <span>Phnom Penh, Cambodia.</span>
        </Col>
        <Col>
          <span>Tel: 085 402 605 (Korean)</span><br></br>
          <span>Emal: info.kshrd@gmail.com phirum.gm@gmail.com</span>
        </Col>
      </Row>
    </div>
  )
}
