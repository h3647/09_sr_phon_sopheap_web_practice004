import React from 'react'
import { Col, Row, Card } from 'react-bootstrap'

export default function UsVision() {
  return (
    <div>
      <Row style={{marginTop: '50px'}}>
        <Col>
          <Card className="text-center">
            <Card.Header><h3>Vision</h3></Card.Header>
            <Card.Body>
              <ul style={{textAlign: 'left'}}>
                <li>To be the best SW Professional Training Center in Cambodia</li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
        <Col>
          <Card className="text-center">
            <Card.Header><h3>Mision</h3></Card.Header>
            <Card.Body>
              <ul style={{textAlign: 'left'}}>
                <li>High quality training and research</li>
                <li>Developing Capacity of SW Experts to be Leaders in IT Field</li>
                <li>Developing sustainable ICT Program</li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      {/* Second Row */}

      <Row style={{marginTop: '30px'}}>
        <Col>
          <Card className="text-center">
            <Card.Header><h3>Strategy</h3></Card.Header>
            <Card.Body>
              <ul style={{textAlign: 'left'}}>
                <li>Best training method with up to date curriculum and environment</li>
                <li>Cooperation with the best IT industry to guarantee student's career and benefits</li>
                <li>Additional Soft Skill, Management, Leadership training</li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
        <Col>
          <Card className="text-center">
            <Card.Header><h3>Slogan</h3></Card.Header>
            <Card.Body>
              <ul style={{textAlign: 'left'}}>
                <li>KSHRD, connects you to various opportunities in IT Field</li>
                <li>Raising brand awareness with continuous advertisement of SNS and any other media</li>
              </ul>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </div>
  )
}
