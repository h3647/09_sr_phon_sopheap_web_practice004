import React from 'react'
import FacebookLogin from 'react-facebook-login';

const responseFacebook = (response) => {
    console.log(response);
  }

export const FacebookLoginComponent = () => {
  return (
    <div>
        <FacebookLogin
    appId="721631978985996"
    autoLoad={true}
    fields="name,email,picture"
    onClick={responseFacebook}
    callback={responseFacebook} />,
    </div>
  )
}
